#include "Cell.h"



void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocuse_receptor_gene = _glocuse_receptor_gene;
	this->_mitochondrion.init();
	this->_atp_units = 0;
}

bool Cell::get_ATP()
{
	std::string rna = this->_nucleus.get_RNA_transcript(this->_glocuse_receptor_gene);
	Protein* prot = this->_ribosome.create_protein(rna);
	if ( prot == NULL)
	{
		std::cerr << "ribosome couldnt create protein";
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*prot);
	bool ans = this->_mitochondrion.produceATP();
	if (ans)
	{
		this->_atp_units = 100;
	}
	return ans;

}