#pragma once
#include "Protein.h"
#include <string>
#include <iostream>

class Ribosome
{
public:
	Protein* create_protein(std::string &RNA_transcript) const;
};

